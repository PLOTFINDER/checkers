
void draw_Grid()
{
  push();
  for(int i = 0; i<8;i++)
  {
      for(int j = 0; j<8;j++)
      {
        if((i+j)%2 == 0)
        {
          fill(255,0,0);
        }
        else
        {
          fill(0);
        }
        
        square(j*80,i*80,80);
      }
  }
  pop();
}
