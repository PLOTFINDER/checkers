class Pawn
{
  PVector position;
  int team;
  Boolean alive;
  PVector[] moves = {null,null};
  Boolean hov = false;
  int dir;
  
  Pawn(int x, int y, int player)
  {
    position = new PVector(x,y);
    team = player;
    dir = team;
    alive = true;
    
  }
  
  void drawIt()
  {
    if(isAlive())
    {
    push();
    
    if(hov)
    {
      fill(0,255,0);
      square(position.x*80,position.y*80,80);
    }
    
    if(getTeam() == 2)
    {fill(255);}
    else
    {fill(0,0,255);}
    
    circle(position.x*80+40,position.y*80+40,50);
    
    pop();
    }
  }
  
  void drawMoves(Pawn[] allPawns)
  {
    moves = calculateMoves(allPawns);
    push();
    

    for(PVector move : moves)
    {
      if(move != null)
      {
      fill(0,255,0);
      square(move.x*80,move.y*80,80);
      }
    }
    
    pop();
    
  }
  
  Boolean movePawn()
  {
    for(PVector move : moves)
    {
      if(move != null)
      {
        if(moveSelected(move))
        {
          setPosition(move.x,move.y);
          return true;
        }
      }
    }
    return false;
  }
  
  int getTeam()
  {
    return team;
  }
  
  PVector getPosition()
  {
    return position;
  }
  
  void setPosition(float x,float y)
  {
    position = new PVector(x,y);
  }
  
  Boolean isAlive()
  {
    return alive;
  }
  
  Boolean isHere(float x, float y)
  {
    return position.x == x && position.y == y && isAlive();
  }
  
  Boolean hover()
  {
    Boolean x = mouseX >= position.x*80 && mouseX <= position.x*80 + 80;
    Boolean y = mouseY >= position.y*80 && mouseY <= position.y*80 + 80;
    
    return x && y;
  }
  
  Boolean selected()
  {
    if(hover() && mousePressed && release && isAlive())
    {
      release = false;
      return true;
    }
    else
    {
      return false;
    }
    
  }
  
  Boolean hoverIt(float px,float py)
  {
    Boolean x = mouseX >= px*80 && mouseX <= px*80 + 80;
    Boolean y = mouseY >= py*80 && mouseY <= py*80 + 80;
    
    return x && y;
  }
  
    Boolean moveSelected(PVector move)
  {
    if(hoverIt(move.x,move.y) && mousePressed && release)
    {
      release = false;
      return true;
    }
    else
    {
      return false;
    }
    
  }
  
  PVector[] calculateMoves(Pawn[] pawns)
  {
    PVector[] moves = new PVector[10];
    
      if(team == 1)
      {
        
        ///////////////////////
        if(position.x-1 >=0 && position.y+1<=8)
        {
          Boolean taken = false;
          for(Pawn aPawn : pawns)
          {
            if(aPawn.isHere(position.x-1,position.y+1))
            {
              taken = true;
              if(position.x-2 >=0 && position.y+2<=8 && aPawn.getTeam() != getTeam())
              {
               Boolean takenSup = false;
               for(Pawn aPawn2 : pawns)
                {
                  if(aPawn2.isHere(position.x-2,position.y+2))
                   {
                     takenSup = true;
                   }
              
                }
                if(!takenSup)
                {
                  moves[0] = new PVector(position.x-2,position.y+2);
                }
            
              }
            }
          }
          if(!taken)
          {
            moves[0] = new PVector(position.x-1,position.y+1);
          }
        }
        ///////////////////////
        if(position.x+1 <=8 && position.y+1<=8)
        {
          Boolean taken = false;
          
          for(Pawn aPawn : pawns)
          {
            if(aPawn.isHere(int(position.x+1),int(position.y+1)))
            {
              taken = true;
              if(position.x+2 >=0 && position.y+2<=8 && aPawn.getTeam() != getTeam())
              {
               Boolean takenSup = false;
               for(Pawn aPawn2 : pawns)
                {
                  if(aPawn2.isHere(position.x+2,position.y+2))
                   {
                     takenSup = true;
                   }
              
                }
                if(!takenSup)
                {
                  moves[1] = new PVector(position.x+2,position.y+2);
                }
            
              }
            }
            
          }
          if(!taken)
          {
            moves[1] = new PVector(position.x+1,position.y+1);
          }
    
        }
      }

      if(team == 2)
      {
        ///////////////////////
        if(position.x-1 >=0 && position.y-1>=0)
        {
          Boolean taken = false;
          for(Pawn aPawn : pawns)
          {
            if(aPawn.isHere(int(position.x-1),int(position.y-1)))
            {
              taken = true;
              
              if(position.x-2 >=0 && position.y-2<=8  && aPawn.getTeam() != getTeam())
              {
               Boolean takenSup = false;
               for(Pawn aPawn2 : pawns)
                {
                  if(aPawn2.isHere(position.x-2,position.y-2))
                   {
                     takenSup = true;
                   }
              
                }
                if(!takenSup)
                {
                  moves[0] = new PVector(position.x-2,position.y-2);
                }
            
              }
            }
            
          }
          if(!taken)
          {
            moves[0] = new PVector(position.x-1,position.y-1);
          }
    
        }
        ///////////////////////
        if(position.x+1 <=8 && position.y-1>=0)
        {
          Boolean taken = false;
          for(Pawn aPawn : pawns)
          {
            if(aPawn.isHere(position.x+1,position.y-1))
            {
              taken = true;
              if(position.x+2 >=0 && position.y-2<=8  && aPawn.getTeam() != getTeam())
              {
               Boolean takenSup = false;
               for(Pawn aPawn2 : pawns)
                {
                  if(aPawn2.isHere(position.x+2,position.y-2))
                   {
                     takenSup = true;
                   }
              
                }
                if(!takenSup)
                {
                  moves[1] = new PVector(position.x+2,position.y-2);
                }
            
              }
            }
            
          }
          if(!taken)
          {
            moves[1] = new PVector(position.x+1,position.y-1);
          }
    
        }
      }
    
    
    return moves;
    
  }
  
}

void mouseReleased()
{
  release = true;
}
