class Player
{
  Pawn[] pawns;
  int team;
  Pawn selection;
  
  Player(Pawn[] somePawns, int player)
  {
    
    pawns = somePawns;
    int team = player;
    
  }
  
  Boolean selectPawn(Pawn[] allPawns)
  {

      for(Pawn pawn : pawns)
      {
        if(pawn.selected())
        { 
          pawn.drawMoves(allPawns);
          selection = pawn;
        }
        else if(pawn.hover())
        {
          pawn.hov = true;
        }
        else
        {
          pawn.hov = false;
        }
      }
    if(selection != null)
    {
      selection.drawMoves(allPawns);
      if(selection.movePawn())
      {
       selection = null;
       return true;
      }
    }
    return false;
  }
  
  void drawPawns()
  {
    for(Pawn pawn : pawns)
    {
      pawn.drawIt();
    }
  }
  
  int getNumberPawns()
  {
    return pawns.length;
  }
  
  Boolean isAtPostion()
  {
    return true;
  }
  
  Boolean isAlive()
  {
    return true;
  }
  
  void removePawn()
  {
  
  }
}
