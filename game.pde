class Game
{
  String board = ".1..1..1..1/1..1..1..1/.1..1..1..1///2..2..2..2/.2..2..2..2/2..2..2..2";
  Player[] players = new Player[2];
  int turn = 0;
  Pawn[] pawns;
  
  Game()
  {
    pawns = place_Pawns(board);
    players = create_Players(pawns);
  }
  
  void update()
  {
    drawPawns();
    selectPawn(pawns);
  }
  
  void drawPawns()
  {
  
    for(Player player : players)
    {
      player.drawPawns();
    }
    
  }
  
  void selectPawn(Pawn[] allPawns)
  {
    if(players[turn].selectPawn(allPawns))
    {
      turn++;
      if(turn >1)
      {
        turn = 0;
      }
    }
  }
  
  Player[] create_Players(Pawn[] pawns)
  {
    Player[] thePlayers = new Player[2];
    
    //Need rework
    Pawn[] teamA = new Pawn[pawns.length / 2];
    Pawn[] teamB = new Pawn[pawns.length / 2];
    int countA = 0;
    int countB = 0;
    
    
    for(Pawn pawn : pawns)
    {
      if(pawn.getTeam() == 1)
      {
        teamA[countA] = pawn;
        countA++;
      }
      else
      {
        teamB[countB] = pawn;
        countB++;
      }
    }
    
    thePlayers[0] = new Player(teamA, 1);
    thePlayers[1] = new Player(teamB, 2);
    
    return thePlayers;
  }
  
  
}

Pawn[] place_Pawns(String board)
{
  
  int x = 0;
  int y = 0;
  int count = 0;
  
    for(char re : board.toCharArray())
  {
     if(isNumeric(str(re)))
     {
       count++;
     }
  }
  
  Pawn[] pawns = new Pawn[count];
  count = 0;
  
  for(char re : board.toCharArray())
  {
    
    if(re == '1')
    {
      pawns[count] = new Pawn(x,y,1);
      count++;
    }
    else if(re == '2')
    {
      pawns[count] = new Pawn(x,y,2);
      count++;
    }
    else if(re == '.')
    {
      x++;
    }
    else if(re == '/')
    {
      y++;
      x=0;
    }
    else
    {
    print("Error '"+re+"' is not used");
    }
    
    
  }
  
  return pawns;
  
}

public static boolean isNumeric(String strNum) {
    if (strNum == null) {
        return false;
    }
    try {
        double d = Double.parseDouble(strNum);
    } catch (NumberFormatException nfe) {
        return false;
    }
    return true;
}
